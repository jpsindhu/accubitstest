import { Component, OnInit,Output,EventEmitter,OnChanges } from '@angular/core';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {

  constructor() { }
  @Output() passValue = new EventEmitter();
  ngOnInit(): void {
  }
  valueChanged(password){
    this.passValue.emit(password)
   }
}
