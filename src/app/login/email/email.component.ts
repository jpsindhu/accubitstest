import { Component, OnInit,Output,EventEmitter,OnChanges } from '@angular/core';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {

  constructor() { }

@Output() emailValue = new EventEmitter();
ngOnInit(): void {
}
valueChanged(email){
 //console.log(email,".............")
 this.emailValue.emit(email)
}
}
