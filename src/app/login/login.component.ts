import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
public email:any;
public password:any;
  constructor(private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
  }
  getemail(em){
    this.email = em
  }
  getpassword(pas){
    this.password = pas;
  }
  login(form){
// service level is not implemented .
// as the backed API is not provide.
    console.log(this.email,"email");
    console.log(this.password,"pass");
    //this.router.navigateByUrl('/home');
    this.router.navigate(['/home']);
  }
}
