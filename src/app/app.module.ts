import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { EmailComponent } from './login/email/email.component';
import { PasswordComponent } from './login/password/password.component';
import { ButtonComponent } from './login/button/button.component';
import { FormsModule } from '@angular/forms';
import { TopComponent } from './home/top/top.component';
import { KeyComponent } from './home/key/key.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    EmailComponent,
    PasswordComponent,
    ButtonComponent,
    TopComponent,
    KeyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
